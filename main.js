// Завдання
// Реалізувати перемикання вкладок (таби) на чистому Javascript.

// Технічні вимоги:
// У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
// Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
// Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.


const tabs = document.querySelector('.tabs');
const tabsContent = document.querySelector('.tabs-content');

tabs.addEventListener('click', (e) => {
    console.log(e);
    const liTabs = e.target.closest('li');
    const liTabsContent = e.target.closest('li');
    let active = document.querySelector('.active');
   if (active){
    active.classList.remove('active');
    findText(e.target);
   } 
   liTabs.classList.add('active');
   console.log('active');
});

function findText(item){
    for (let i of tabsContent.children){
        if(i.dataset.article === item.dataset.article ){
            i.classList.add('active');
            i.classList.remove('disable');

        }else{
            i.classList.remove('active');
            i.classList.add('disable');
        }
    }
}


